This is an example app on how to use the [Scroll Content Management API](https://bitbucket.org/K15t/scroll-content-management-api).

The app sends out an email to all reviewers when the workflow status of a page is changed to "under review".

How the app works is described in [this tutorial](https://www.k15t.com/display/_PK/VSNDOC/workflow-notification-tutorial).