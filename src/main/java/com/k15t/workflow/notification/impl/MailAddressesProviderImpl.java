package com.k15t.workflow.notification.impl;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserKey;
import com.k15t.scroll.scm.api.SpaceRoles;
import com.k15t.scroll.scm.api.SpaceRolesService;
import com.k15t.workflow.notification.MailAddressesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


@Component
public class MailAddressesProviderImpl implements MailAddressesProvider {

    @ComponentImport
    @Autowired
    private SpaceRolesService spaceRolesService;
    @ComponentImport
    @Autowired
    private UserAccessor userAccessor;



    @Override
    public Set<String> getReviewerAddresses(String spaceKey) {
        Set<String> addresses = new HashSet<>();
        SpaceRoles spaceRoles = spaceRolesService.getConfiguredSpaceRoles(spaceKey);

        for (String groupName : spaceRoles.getReviewerGroupNames()) {
            for (ConfluenceUser user : userAccessor.getMembers(userAccessor.getGroup(groupName))) {
                addresses.add(user.getEmail());
            }
        }

        for (String userKey : spaceRoles.getReviewerUserKeys()) {
            ConfluenceUser user = userAccessor.getUserByKey(new UserKey(userKey));
            addresses.add(user.getEmail());
        }

        return addresses;
    }

}
