package com.k15t.workflow.notification.impl;

import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.k15t.workflow.notification.MailService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;


@Component
public class MailServiceImpl implements MailService {

    @ComponentImport
    @Autowired
    private MultiQueueTaskManager taskManager;


    @Override
    public void sendMail(Set<String> toAddresses, String subject, String htmlBody) {
        taskManager.addTask("mail", new ConfluenceMailQueueItem(concatAddresses(toAddresses), subject, htmlBody, MIME_TYPE_HTML));
    }


    private String concatAddresses(Set<String> addresses) {
        return StringUtils.join(addresses, ", ");
    }

}
