package com.k15t.workflow.notification.impl;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.k15t.scroll.scm.api.events.page.WorkflowStatusChangeEvent;
import com.k15t.workflow.notification.MailContentProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;


@Component
public class MailContentProviderImpl implements MailContentProvider {

    @ComponentImport
    @Autowired
    private TemplateRenderer templateRenderer;
    @ComponentImport
    @Autowired
    private PageManager pageManager;
    @ComponentImport
    @Autowired
    private SettingsManager settingsManager;


    @Override
    public String getMailBody(WorkflowStatusChangeEvent event) {
        Page page = pageManager.getPage(event.getScrollPage().getConfluencePageId());

        Map<String, Object> context = new HashMap<>();
        context.put("link", (settingsManager.getGlobalSettings().getBaseUrl() + page.getUrlPath()));
        context.put("space", page.getSpace().getDisplayTitle());
        context.put("spaceUrl", (settingsManager.getGlobalSettings().getBaseUrl() + page.getSpace().getUrlPath()));
        context.put("pageName", event.getScrollPage().getScrollPageTitle());
        context.put("pageVersion", event.getScrollPage().getVersion().getName());
        StringWriter writer = new StringWriter();
        try {
            templateRenderer.render("/com/k15t/workflow/notification/mail.vm", context, writer);
        } catch (IOException e) {
            throw new RuntimeException("Could not render the mail body.", e);
        }
        return writer.toString();
    }


    @Override
    public String getMailSubject(WorkflowStatusChangeEvent event) {
        String pageTitle = event.getScrollPage().getScrollPageTitle();
        return String.format("Page '%s' is ready for approval.", pageTitle);
    }

}
