package com.k15t.workflow.notification;

import com.k15t.scroll.scm.api.events.page.WorkflowStatusChangeEvent;


public interface MailContentProvider {

    String getMailBody(WorkflowStatusChangeEvent event);


    String getMailSubject(WorkflowStatusChangeEvent event);

}
