package com.k15t.workflow.notification;

import java.util.Set;


public interface MailService {

    void sendMail(Set<String> toAddresses, String subject, String htmlBody);

}
