package com.k15t.workflow.notification;


import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.k15t.scroll.scm.api.WorkflowStatus;
import com.k15t.scroll.scm.api.events.page.WorkflowStatusChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import java.util.Set;


@Component
public class WorkflowStatusChangeEventListener {

    @ComponentImport
    @Autowired
    private EventPublisher eventPublisher;
    @Autowired
    private MailService mailService;
    @Autowired
    private MailAddressesProvider mailAddressesProvider;
    @Autowired
    private MailContentProvider mailContentProvider;


    @PostConstruct
    public void init() {
        eventPublisher.register(this);
    }


    @PreDestroy
    public void destroy() {
        eventPublisher.unregister(this);
    }


    @SuppressWarnings("unused")
    @EventListener
    public void onWorkflowStatusChangeEvent(WorkflowStatusChangeEvent event) {
        if (pageMovedToApproval(event)) {
            Set<String> reviewerAddresses = mailAddressesProvider.getReviewerAddresses(event.getScrollPage().getSpaceKey());
            String content = mailContentProvider.getMailBody(event);
            String subject = mailContentProvider.getMailSubject(event);
            mailService.sendMail(reviewerAddresses, subject, content);
        }
    }


    private boolean pageMovedToApproval(WorkflowStatusChangeEvent event) {
        return WorkflowStatus.approval == event.getNewStatus();
    }

}
