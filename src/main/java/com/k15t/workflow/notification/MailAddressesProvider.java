package com.k15t.workflow.notification;

import java.util.Set;


public interface MailAddressesProvider {

    Set<String> getReviewerAddresses(String spaceKey);

}
